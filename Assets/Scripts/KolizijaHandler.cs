﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KolizijaHandler : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    public GameObject explosion;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Slinavac")
        {
            Debug.Log("Lorki je nastradao");
        }

        if (collision.gameObject.tag == "GlavaSlinavac")
        {
            Debug.Log("Lorki je ubio slinavca");
            //Destroy(collision.gameObject);
            rigidBody.velocity = new Vector2(0, 25);
            //Explode(GameObject.Find("Slinavac").transform.position);
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(GameObject.Find("Slinavac"));
        }

        if(collision.gameObject.tag == "Ball")
        {
            Debug.Log("Ball udario lorkija");
            Destroy(gameObject); // ucitaj gameover nesto
        }
    }


   

}

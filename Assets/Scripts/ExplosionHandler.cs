﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(EksplodirajZatimNestani());
    }


    IEnumerator EksplodirajZatimNestani()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject); 
    }


}

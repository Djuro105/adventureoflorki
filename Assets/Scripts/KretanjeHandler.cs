﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KretanjeHandler : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 3.0f;
    [SerializeField] private float jumpSpeed = 6.0f;

    private Rigidbody2D rigidBody;
    Collider2D myCollider2D;

    private Vector2 movement = new Vector2();

    private bool facingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        myCollider2D = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
    }

    private void FixedUpdate()
    {
        MovementHandler();
    }

    private void MovementHandler()
    {
        movement.x = Input.GetAxisRaw("Horizontal") * movementSpeed;
        movement.y = rigidBody.velocity.y;
        rigidBody.velocity = movement;

        if (movement.x < 0 && facingRight) FlipSides();
        if (movement.x > 0 && !facingRight) FlipSides();
    }

    private void Jump()
    {
        // ako ne dodiruje ground, ne skace
        if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground"))) { return; }

        // stisnem space dodaje se velocity rigidbodyu prema gore 
        if (Input.GetKeyDown("space"))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            rigidBody.velocity += jumpVelocityToAdd;
        }
    }

    private void FlipSides()
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180);
    }


}

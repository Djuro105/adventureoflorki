﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pcela : MonoBehaviour
{
    private Animator myAnimator;
    private string animationParameter = "AnimationParameterPcela";

    public int numberOfBalls = 5;
    public GameObject ball;
    private List<GameObject> objectPool;

    enum AnimationStatesPcela {
        idle = 0,
        shoot = 1
    }

    void Start()
    {
        myAnimator = GetComponent<Animator>();
        
        objectPool = new List<GameObject>(numberOfBalls);
        for (int i = 0; i < numberOfBalls; i++)
        {
            GameObject obj = Instantiate(ball);
            obj.SetActive(false);
            objectPool.Add(obj);
        }

        StartCoroutine(PucajAnimirajPcela());

    }

    void Update()
    {
        //AnimationHandlerPcela();
    }

    private GameObject GetBallObject()
    {
        for (int i = 0; i < objectPool.Count; i++)
        {
            if (!objectPool[i].activeInHierarchy)
            {
                return objectPool[i];
            }
        }
        return null;
    }


    IEnumerator PucajAnimirajPcela()
    {
        while (true)
        {
         myAnimator.SetInteger(animationParameter, (int)AnimationStatesPcela.idle);   // idle stanje  2 sekunde
         yield return new WaitForSeconds(3);

         myAnimator.SetInteger(animationParameter, (int)AnimationStatesPcela.shoot);   // pucanje stanje x sekundi
         yield return new WaitForSeconds(0.32f);
         IspaliBall();
         yield return new WaitForSeconds(0.02f);
           
            
        }
    }


    private void IspaliBall()
    {
        Transform shootPosition = this.gameObject.transform.GetChild(0);
        GameObject ironBall = GetBallObject();
        ironBall.transform.position = shootPosition.position;
        ironBall.SetActive(true);
    }

    /* private void AnimationHandler()
     {
         if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground"))) //nijeNaGroundu
         {
             animator.SetInteger(animationParameter, (int)AnimationStates.jump);
         }
         else if (hodanjePoHorizontali != 0)  //run
         {
             animator.SetInteger(animationParameter, (int)AnimationStates.run);
         }
         else
         {
             animator.SetInteger(animationParameter, (int)AnimationStates.idle);
         }
     }*/



}

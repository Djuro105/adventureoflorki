﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    Collider2D myCollider2D;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        myCollider2D = GetComponent<Collider2D>();
        rigidBody.gravityScale = 0;

        myCollider2D.enabled = true;

        StartCoroutine(GoreDoljeRock());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator GoreDoljeRock()
    {
        while (true)
        {

            rigidBody.velocity = new Vector2(0, 7); //gore
            yield return new WaitForSeconds(1.5f);

            rigidBody.velocity = new Vector2(0, -7); //dolje
            yield return new WaitForSeconds(1.5f);        
            
        }
    }

}
